import React from 'react'
import { BrowserRouter, Route } from "react-router-dom";

// Views
import HomeView from '../view/HomeView/HomeView'

const MainRoutes = () => (
  <BrowserRouter>
    <Route exact path="/" component={HomeView} /> 
  </BrowserRouter>
)

export default MainRoutes;
