import React, { Component } from 'react'
import { Badge } from '@chakra-ui/core'
import Topbar from '../../components/Topbar/Topbar'

class HomeView extends Component {
render() {
  return (
      <div>
        <Topbar />
        <Badge variantColor="purple">New</Badge>
      </div>
    )
  }
}

export default HomeView;
